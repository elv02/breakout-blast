using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public int score;
    public float timeElapsed;
    public int brickCount;
    public int playerLives;
    public GameState state;
    public enum GameState { WON, LOST, PAUSED, RUNNING }
    // Private instantiated (non duplicate) instance of Game Manager
    private static GameManager _instance;
    // Access the GameManager singleton.
    public static GameManager instance { get { return _instance; } }
    private Text UIScore;
    private Text UITimer;
    private Text UILives;
    // Awake is called on script load
    void Awake()
    {
        if(_instance != null && _instance != this)
        {
            Debug.LogWarning("Warning: Attempted construction of duplicate Singleton " + this.name);
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        timeElapsed = 0f;
        playerLives = 3;
        state = GameState.RUNNING;
        brickCount = GameObject.FindGameObjectsWithTag("Brick").Length;

        UIScore = GameObject.Find("ScoreCounter").GetComponent<Text>();
        UILives = GameObject.Find("LivesCounter").GetComponent<Text>();
        UITimer = GameObject.Find("Timer").GetComponent<Text>();

        UIScore.text = "Score: " + score;
        UILives.text = "Lives: " + playerLives;
        UITimer.text = "Timer: " + timeElapsed;
    }

    // Update is called once per frame
    void Update()
    {
        // Update time elapsed here.
        timeElapsed += Time.deltaTime;
        int seconds = (int)(timeElapsed % 60);
        UITimer.text = "Timer: " + seconds;
    }

    // Call this function from a brick when it is completely removed from the board
    public void BrickDestroyed()
    {
        brickCount--;
        if(brickCount <= 0)
        {
            state = GameState.WON;
            Debug.Log("You Win!");
        }
        UIScore.text = "Score: " + score;
    }
    // Call this function when a ball travels out of bounds
    public void BallLost()
    {
        playerLives--;
        if(playerLives <= 0)
        {
            state = GameState.LOST;
            Debug.Log("Game Over! You Lost!");
            CloseGame();
        }
        UILives.text = "Lives: " + playerLives;
    }

    public void CloseGame(){
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif 
    }
}
