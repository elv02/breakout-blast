using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Ball : MonoBehaviour
{    
    // Force multiplier to apply to the ball after it's spawned in
    public float initialBallSpeed = 1500f;
    // Paddle requires a RigidBody2D to perform physics collisions.
    private Rigidbody2D body;
    // Start is called before the first frame update
    void Start()
    {
        // Retrieve component references
        if(TryGetComponent(out Rigidbody2D found))
        {
            body = found;
            Invoke("StartMoving", 3f);
        } 
        else 
        {
            Debug.LogError("ERROR: Player paddle object has no attached RigidBody2D component!  Cannot perform physics collisions!");
        }
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.name == "KillZone")
        {
            GameManager.instance.BallLost();
            body.velocity = Vector2.zero;
            transform.position = Vector2.zero; // TODO: Make sure this resets to the ball start position (set up as configurable var?)
            Invoke("StartMoving", 3f);
        }
    }

    private void StartMoving()
    {
            Vector2 force = new Vector2(Random.Range(-1f, 1f), Random.Range(0.25f, 1f)).normalized * 1500;
            body.AddForce(force);
    }
}
