using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Brick : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        // TODO: When Game Manager singleton is in, come back here to increase brick counter!
        // Retrieve component references
        if(TryGetComponent(out SpriteRenderer spr))
        {
            spr.color = new Color(Random.Range(0.25f, 0.75f), Random.Range(0.25f, 0.75f), Random.Range(0.25f, 0.75f), 1);
        } 
        else 
        {
            Debug.LogError("Brick object exists with no SpriteRender component! Is it invisible?");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.name == "Ball")
        {
            GameManager.instance.score += 10;
            GameManager.instance.BrickDestroyed();
            Destroy(gameObject);
        }
    }
}
