/*******************************\
 * Player Control Script       *
 * ---------------------       *
 * Handles player input while  *
 * the game is active.         *
\*******************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Rigidbody2D))]
public class Player : MonoBehaviour
{
    // Reference to the InputActionAsset for all game inputs (including player control)
    public InputActionAsset inputs;
    // Amount of force behind player input on the physics based paddle
    public float force = 24; 
    // Paddle requires a RigidBody2D to perform physics collisions.
    private Rigidbody2D body;
    private InputAction move;
    private InputAction menu;
    // Horizontal direction of the player (read from analogue/digital input).
    private float direction;
    // Awake is called when the script is loaded
    void Awake()
    {
        // Set up our input bindings
        move = inputs.FindAction("Move");
        move.performed += OnMove;
        move.canceled += OnMove;
        move.Enable();

        menu = inputs.FindAction("Menu");
        menu.performed += OnMenu;
        menu.Enable();
    }

    // Start is called before the first frame update
    void Start()
    {
        // Retrieve component references
        if(TryGetComponent(out Rigidbody2D found))
        {
            body = found;
        } 
        else 
        {
            Debug.LogError("ERROR: Player paddle object has no attached RigidBody2D component!  Cannot perform physics collisions!");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(direction != 0)
        {
            Vector2 moveForce = Vector2.right * direction * force;
            body.AddForce(moveForce);
        }
    }

    void OnDestroy()
    {
        // Important: deregister our input callbacks to prevent memory leaks!   
        move.Disable();
        move.performed -= OnMove;
        move.canceled -= OnMove;
        menu.Disable();
        menu.performed -= OnMenu;
    }

    private void OnMove(InputAction.CallbackContext context)
    {
        direction = context.ReadValue<float>();
    }
    private void OnMenu(InputAction.CallbackContext context)
    {
        if(GameManager.instance.state == GameManager.GameState.PAUSED)
        {
            GameManager.instance.state = GameManager.GameState.RUNNING;
            Time.timeScale = 1f;
        }
        else
        {
            GameManager.instance.state = GameManager.GameState.PAUSED;
            Time.timeScale = 0f;
        }
        
    }
}
